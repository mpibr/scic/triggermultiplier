#define INPUT_TTL_PIN 3
#define OUTPUT_TTL_PIN 11

int inputInterval = 0;
int outputInterval = 0;
int pulseWidth = 0;
int ttlFactor = 4; //e.g. the led is triggered at 4*the camera

void setup() {
  pinMode(INPUT_TTL_PIN, INPUT);
  pinMode(OUTPUT_TTL_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(9600); // Initialize serial communication at 9600 bits per second
  Serial.println("Trigger multiplier v0.1");

  for(int pin=2;pin<=10;pin++){
    if(pin != INPUT_TTL_PIN){
      pinMode(pin, INPUT);
      if(digitalRead(pin)){
        ttlFactor = pin;
      }
    }
  }
  Serial.print("Pin ");
  Serial.print(ttlFactor);
  Serial.print(" is HIGH, setting trigger factor x");
  Serial.println(ttlFactor);

  //No framerate given. Measuring and setting automatically
  if(inputInterval==0){
    Serial.println("No framerate defined. Measuring camera framerate!");
    unsigned long detectionStartTime = millis();
    
    //If camera is currently being triggered, wait
    if(digitalRead(INPUT_TTL_PIN)){
      while(digitalRead(INPUT_TTL_PIN)){ 
      }
    }
    
    //Wait for signal
    while(!digitalRead(INPUT_TTL_PIN)){
    }
    
    //Start measurement
    unsigned long signalStart = millis(); // Get the current time
    while(digitalRead(INPUT_TTL_PIN)){
    }
    unsigned long signalStop = millis(); // Get the current time
    
    //Wait for next signal
    while(!digitalRead(INPUT_TTL_PIN)){
    }
    unsigned long nextSignalStart = millis(); // Get the current time

    //Calculate input interval and pulsewidth
    inputInterval = nextSignalStart - signalStart; // Calculate the interval
    pulseWidth = signalStop - signalStart;
  }

  //Calculate output interval
  outputInterval = inputInterval/ttlFactor;
  Serial.print("TTL INPUT interval: ");
  Serial.print(inputInterval);
  Serial.println("ms");
  Serial.print("TTL OUTPUT interval: ");
  Serial.print(outputInterval);
  Serial.print("ms (1/");
  Serial.print(ttlFactor);
  Serial.println(" TTL INPUT)");
  Serial.print("Pulsewidth: ");
  Serial.print(pulseWidth);
  Serial.println("ms");
}

void loop() {
  if (digitalRead(INPUT_TTL_PIN)) {
    digitalWrite(LED_BUILTIN, HIGH);

    for(int i=0;i<ttlFactor-1;i++){
      digitalWrite(OUTPUT_TTL_PIN, HIGH);
      delay(pulseWidth);
      digitalWrite(OUTPUT_TTL_PIN, LOW);
      delay(outputInterval - pulseWidth);
    }
    digitalWrite(OUTPUT_TTL_PIN, HIGH);
    delay(pulseWidth);
    digitalWrite(OUTPUT_TTL_PIN, LOW);
    digitalWrite(LED_BUILTIN, LOW);

  }
}
