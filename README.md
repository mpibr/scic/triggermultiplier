# TriggerMultiplier

Arduino code to trigger a device at a multiple of an incoming trigger signal.

Measures the interval of an incoming TTL signal on startup/reset on a given Pin (input).
Generates a sequence of TTL pulses on an given output pin.

The multiplier can be set by bridging a specific pin (pins 2 to 10) to high (5V).
E.g. in order to emit 4 consecutive triggers for each incoming TTL, Pin 4 should be connected to 5V.
Every time a TTL is detected on input pin, 4 TTL pulses are generated on the output pin.

The interval of the outgoing TTL pulses is calculated as inputInterval/multiplier.
E.g. a 25Hz Signal will have an interval of 40ms. A multiplier of 4 will generate 4 pulses with an interval of 10ms
for each detected incoming TTL.
